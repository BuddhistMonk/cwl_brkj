import Vue from 'vue'
import uView from 'uview-ui'
import store from './store'
import App from './App'
Vue.use(uView);

Vue.config.productionTip = false

App.mpType = 'app'

// 引入uView提供的对vuex的简写法文件
let vuexStore = require('@/store/$u.mixin.js')
Vue.mixin(vuexStore)

const app = new Vue({
	...App,
	store
})
app.$mount()
