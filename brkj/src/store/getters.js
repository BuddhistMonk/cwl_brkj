const getters = {
	lang: state => state.app.lang,
	token: state => state.user.token,
	systemTypes: state => (systemTypeEnmu) => {
		console.log(systemTypeEnmu)
		const list = state.system.systemTypes.filter(item => item.typeEnmu === systemTypeEnmu)
		console.log(list)
		return list
	},
	systemTypeDefault:state=>(systemTypeEnmu)=>{
		const list = state.system.systemTypes.filter(item => item.typeEnmu === systemTypeEnmu)
		let st=null;
		list.forEach(item=>{
			if(item.isDefault){
				st=item;
			}
		})
		if(st==null){
			if(list.length>0){
				st=list[0];
			}
		}
		return st;
	},
	isGranted: state => (policyName) => {
		return state.app.appConfig.auth.policies[policyName] != undefined && state.app.appConfig.auth.grantedPolicies[
			policyName] != undefined;
	},
	vuex_tabbar: state => state.vuex_tabbar
}

export default getters
