import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters.js'

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
	},
	state: {
		vuex_tabbar: [{
				iconPath: "home",
				selectedIconPath: "home-fill",
				text: '首页',
				pagePath: '/pages/index/index'
			},
			{
				iconPath: "chat",
				selectedIconPath: "chat-fill",
				text: '客户',
				pagePath: '/pages/drivers/recordList'
			},
			{
				iconPath: "bag",
				selectedIconPath: "bag-fill",
				text: '任务',
				pagePath: '/pages/drivers/records',
				midButton: false
			},
			{
				iconPath: "grid",
				selectedIconPath: "grid-fill",
				text: '应用',
				pagePath: '/pages/drivers/tasks'
			},
			{
				iconPath: "account",
				selectedIconPath: "account-fill",
				text: "我",
				pagePath: '/pages/account/loginOut'
			}
		],
		app:{
			inactiveColor: "909399",
			activeColor: "#5098FF",
			midButton: false,
			currentTabUrl: '/pages/index/index'
		}
		
	},
	mutations: {

	},
	actions: {

	},
	getters
})

export default store
