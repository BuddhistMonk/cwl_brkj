import Request from 'luch-request'
import {
	baseURL,
	ACCESS_TOKEN
} from '@/common/consts.js'

const api = new Request({
	baseURL: baseURL
})

api.interceptors.request.use((config) => { // 可使用async await 做异步操作
	uni.showLoading({
		title: '请稍后...'
	})
	const token = uni.getStorageSync(ACCESS_TOKEN)
	config.header = {
		...config.header
	}
	return config
}, config => {
	uni.hideLoading()
	return Promise.reject(config)
})

api.interceptors.response.use((response) => {
	/* 对响应成功做点什么 可使用async await 做异步操作*/
	uni.hideLoading()
	console.log(response)
	if (response.data.status == 'error') {
		uni.showToast({
			icon: 'none',
			title: response.data.msg,
		})
		return Promise.reject(response)
	}

	return response.data
}, (response) => {
	/*  对响应错误做点什么 （statusCode !== 200）*/
	uni.hideLoading()
	const error = {
		response: response
	}
	if (error.response.statusCode !== 200) {
		uni.showToast({
			icon: 'none',
			title: '服务器错误，请联系管理员',
		})
	}
	return Promise.reject(response)
})

//api.interr
export default api
