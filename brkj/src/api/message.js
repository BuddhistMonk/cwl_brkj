import api from '../common/api.js'

export const getCode = (params) => {
	return api.get('/Ajax/Message.ashx?action=code', {
		params
	}).then(({
		data
	}) => data)
}
