import api from '../common/api.js'

const url = "/Ajax/Login.ashx?action="

export const projectBind = () => {
	return api.get(url + `ProjectBind`).then(({
		data
	}) => data)
}

export const login = (params) => {
	return api.get(url + `CodeLogin`, {
		params
	}).then(({
			data
		}) =>
		data
	)
}
